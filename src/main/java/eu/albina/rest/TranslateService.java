/*******************************************************************************
 * Copyright (C) 2019 Norbert Lanzanasto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package eu.albina.rest;

import java.security.Principal;
import java.util.List;
import java.util.Vector;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.openjson.JSONArray;
import com.github.openjson.JSONObject;
import com.mysql.cj.xdevapi.JsonArray;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import eu.albina.exception.AlbinaException;
import eu.albina.model.TranslateConfig;
import eu.albina.model.enumerations.Role;
import eu.albina.rest.filter.Secured;
import eu.albina.util.HttpClientUtil;
import eu.albina.util.HibernateUtil;
import io.swagger.v3.oas.annotations.tags.Tag;

@Path("/translate")
@Tag(name = "translate")
public class TranslateService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	private final Client client = HttpClientUtil.newClientBuilder().build();

	@Context
	UriInfo uri;

	@POST
	@Secured({ Role.FORECASTER, Role.FOREMAN, Role.OBSERVER, Role.ADMIN })
	@SecurityRequirement(name = AuthenticationService.SECURITY_SCHEME)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Operation(summary = "Translate string to EN")
	//@ApiResponse(description = "translations", content = @Content(array = @ArraySchema(schema = @Schema(implementation = String[]))))
	//API response is JSON array of texts
	public Response getTranslation(
		Vector<String> texts 
	) {
		logger.debug("GET translation");
		try {
			TranslateConfig conf = HibernateUtil.getInstance().runTransaction(entityManager ->
				entityManager.createQuery("from TranslateConfig as c", 
				TranslateConfig.class).getSingleResult());

			WebTarget request = client.target(conf.getUrl())
				.queryParam("target_lang", "EN")
				.queryParam("source_lang", "SK");
			for(String text : texts) {
				//logger.info("TXT: {}", text);
				request = request.queryParam("text", text);
			}
			//logger.debug("Start date for {}: {}", config.getBlogId(), lastFetch.get(config.getBlogId()).toString());
			String deepApiKey = conf.getKey();

			String translations = request.request()
				.header("Authorization", deepApiKey)
				.get(String.class);
			//String translationJson =  String.format("{translation:${name}}", translation);
			
			JSONArray responseArr = new JSONArray();

			//itrerate over deepl results and put in into one array
			JSONObject obj = new JSONObject(translations);
			JSONArray arr = obj.getJSONArray("translations");
			for (int i = 0; i < arr.length(); i++) {
				String translation =  arr.getJSONObject(i).getString("text");
				//logger.info("TRANS: {}", translation);
				responseArr.put(translation);
			}
			
			String response = responseArr.toString();

			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
		}
	}


}
